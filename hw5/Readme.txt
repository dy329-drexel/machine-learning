Homework 5
Cs 383
Deepak Yadav
3/3/2018
----------------------------------------------------------------------
homework5 is the pdf containing stats for part 2 and 3 and matrix for part4
run-  part1.m for SVM 
		part2.m for multi class SVM

----------------------------------------------------------------------------------------
you need these function files in same directory 

randomizeData.m: it randomize data with seed 0
getModel.m: train SVM model using our standard training data
-----------------------------------------------------------------------------
no Issues make sure the data files CTG and spambase.data is in the same directory.

-----------------------------------------------------------------------------------------------------------------------------
I prvided all data/data folders in Hw5.zip folder .