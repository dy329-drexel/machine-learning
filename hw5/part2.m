%clear and specify data file
clear ; close all;
filename = 'CTG.csv';
datafile = 'CTG.mat';

if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file,  the 2nd to last column is to be discarded, and the last column contains the target class.
    data = csvread(filename, 2, 0);
    data(:, end-1) = [];
    save(datafile,'data');
end

% randomizes the data using the function

data=randomizeData(data);


colsize = size(data,2);
% selects the first 2/3 rd data for training and remaining 1/3 for testing
no = ceil( length(data) * 2 / 3 );
data_training = data(1 : no, :);

% set the remaining for testing
data_testing = data(no+1 : end, :);


%find mean and sd of the data
mv = mean(data_training(:, 1:end-1));
sd = std(data_training(:, 1:end-1));

% standardizes data
std_data_training = [(data_training(:, 1:end-1) - mv) ./ sd, data_training(:, end)];
std_data_testing = [(data_testing(:, 1:end-1) - mv) ./ sd, data_testing(:, end)];


%custom function for train a SVM using our triaing data
Model=getModel(data,std_data_training);

% for every SVM models, predict the class
Prediction = zeros( size(std_data_testing, 1), length(Model) );
for i = 1 : length(Model)
    Prediction(:, i) = predict(Model{i}, std_data_testing(:, 1:end-1));
end

% Find the class choose small one in case of tie
Prediction = mode(Prediction , 2);

% calculate accuracy
correct = 0;
for i = 1 : length(Prediction)
    if (Prediction(i) == std_data_testing(i, end))
        correct = correct + 1;
    end
end


%compute prediction matrix

[C,order] = confusionmat(Prediction,std_data_testing(:, end));
C=C/length(Prediction);
C=C*100;
accuracy = correct / length(Prediction);
% print accuracyfor our results
fprintf('Accuracy is: %f\n', accuracy);

disp('The confusion matrix is:');
disp(C);

fprintf('length  of prediction matrix: p%f\n', length(Prediction));



