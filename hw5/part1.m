
%clear and specify data file
clear ; close all; 
filename = 'spambase.data';
datafile = 'spambase.mat';



if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file
    data = csvread(filename);

    save(datafile,'data');
end



% randomizes the data using the function

data=randomizeData(data);


colsize = size(data,2);
% selects the first 2/3 rd data for training and remaining 1/3 for testing
no = ceil( length(data) * 2 / 3 );
data_training = data(1 : no, :);

% set the remaining for testing
data_testing = data(no+1 : end, :);


%find mean and sd of the data
mv = mean(data_training(:, 1:end-1));
sd = std(data_training(:, 1:end-1));

% standardizes data
std_data_training = [(data_training(:, 1:end-1) - mv) ./ sd, data_training(:, end)];
std_data_testing = [(data_testing(:, 1:end-1) - mv) ./ sd, data_testing(:, end)];

%using standard library to train a SVM using our triaing data
Model=fitcsvm(std_data_training(:, 1:end-1), std_data_training(:, end));

%getting predictions
Predict=predict(Model, std_data_testing(:, 1:end-1));

%compute our evaluation statistics 
 tp = 0;
 fp = 0;
 fn = 0;
 tn = 0;
 
 for i = 1 : length(Predict)
    if Predict(i) == 1
        if std_data_testing(i, end) == 1
            tp = tp + 1;
        else
            fp = fp + 1;
        end
    else
        if std_data_testing(i, end) == 0
            tn = tn + 1;
        else
            fn = fn + 1;
        end
    end
end

 
 
 %calculate our performance for the SVM
 
 
precision = tp / (tp + fp);
recall = tp / (tp + fn);
fmeasure = 2 * precision * recall / (precision + recall);
accuracy = (tp + tn) / (tp + tn + fp + fn);

% Print out the performance results
fprintf('Precision is: %f\n', precision);
fprintf('Recall is: %f\n', recall);
fprintf('F-Measure is: %f\n', fmeasure);
fprintf('Accuracy is : %f\n', accuracy);



