function [Model] = getModel(~,std_data_training)

max1 = max(std_data_training(:, end));

% saprate data based on class label
class = cell(max1, 1);
for i = 1 : max1
    class{i} = std_data_training( std_data_training(:, end) == i , :);
end

% train SVM models using standard library and save it into variable model
Model = cell(0, 1);
for i = 1 : length(class) - 1
    for j = i + 1 : length(class)
        data = [class{i}; class{j}];
        Model{end + 1} = fitcsvm(data(:, 1:end-1), data(:, end));
        
    end
end

end