function [X] = randomizeData(X)

%set seeed to 0 and randomize data

rng(0);
X = X( randperm( length(X) ), : );

end