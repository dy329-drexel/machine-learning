function [X] = randomizeData(X)

%set seeed to 0 and randomize data

rng(0);
order =  randperm( length(X) );
X = X(order, :);
end