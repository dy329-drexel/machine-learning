Homework 6
Cs 383
Deepak Yadav
3/17/2018
----------------------------------------------------------------------
homework6 is the pdf containing stats for part 2 and 3 and 4 as well as extra credit
run-  part1.m for Binary ANN with accuracy graph
part2.m for Recall precision graph
part3.m Multi class ANN

----------------------------------------------------------------------------------------
you need these function files in same directory 

randomizeData.m: it randomize data with seed 0
getAccuracy: function which gets accuracy for each iteration in binary ANN
getAccuracyMultiClass: function which gets accuracy for each iteration in Multi classs ANN
-----------------------------------------------------------------------------
no Issues make sure the data files CTG and spambase.data is in the same directory.

-----------------------------------------------------------------------------------------------------------------------------
I prvided all data/data folders in Hw5.zip folder .