function [accuracy] = getAccuracy(O, Y)

%calculate accuracy 
    len = length(O);
    correct = 0;
    for i = 1:len
        r1 = O(i, 1);
        r2 = Y(i, 1);
        
       
        if(r1 > 0.5)
          
            if (r2 == 1)
               correct = correct + 1; 
            end
        else
            if(r2 == 0)
                correct = correct + 1;
            end
        end
    end
    
    accuracy = correct/len;
end