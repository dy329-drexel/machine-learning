
%clear and specify data file
clear ; close all; 
filename = 'CTG.csv';
datafile = 'input.mat';

if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file
    data = csvread(filename,2);

    save(datafile,'data');
end

datat=data(:,1:end-2);
data=[datat,data(:,end)];

% randomizes the data using the function

randomData=randomizeData(data);

colsize = size(randomData,2);
% selects the first 2/3 rd data for training and remaining 1/3 for testing
no = ceil( length(randomData) * 2 / 3 );
data_training = randomData(1 : no, :);

% set the remaining for testing
data_testing = randomData(no+1 : end, :);

%find mean and sd of the data
%find mean and sd of the data
mv = mean(data_training(:, 1:end-1));
sd = std(data_training(:, 1:end-1));

%select last column as label
testY = data_testing(:, colsize);
Y = data_training(:, colsize);


%standardize the data
stdData = data_training(: , 1:colsize-1);

testData = data_testing(: , 1:colsize-1);

stdData = (stdData-mv)./sd;
testData = (testData-mv)./sd;

%find length of the data

N = length(stdData);


% add bias to train and test data
stdData = [stdData, ones(N,1)];
X = stdData;
testData = [testData, ones(length(testData), 1)];
testX = testData;

%set iterations and learning parameter
i = 1000;
learn = 0.5;
D = size(stdData,2);

hidden_layer_size = 20;

%since there are 3 class labels
K = 3;

%set weights randomly in range -1 , 1
R = [-1 1];
weight1 = rand(D, 20)*range(R)+min(R);
weight2 = rand(20, K)*range(R)+min(R);
% Encode Y Values
newY = [];
for i = 1:size(Y)
   if Y(i) == 1
       newY = [newY; 1 0 0];
   elseif Y(i) == 2
       newY = [newY; 0 1 0];
   else
       newY = [newY; 0 0 1];
   end
end
new_testY = [];
for i = 1:size(testY)
   if testY(i) == 1
       new_testY = [new_testY; 1 0 0];
   elseif testY(i) == 2
       new_testY = [new_testY; 0 1 0];
   else
       new_testY = [new_testY; 0 0 1];
   end
end

accuracy = zeros(i, 1);
for i = 1:i

    %Propagate data forward to get
    H = X * weight1;
    H = 1 ./ (1 + exp(-1 .* H));
    
    O = H * weight2;
    O = 1 ./ (1 + exp(-1 .* O));
    %Computer the error at the output layer 
    EOUT = newY - O;
    %Update the weights from hidden layer to output laye
    weight2 = weight2 + (learn/N) * H.' * EOUT;
    
    EHIDDEN = EOUT * weight2.' .* H .* (1 - H);
    %Compute the error at the hidden layer
    weight1 = weight1 + (learn/N) .* X.' * EHIDDEN;
    
    % calculate accuracy

    accuracy(i, 1) =  getAccuracyMultiClass(O, newY);
end

plot(accuracy, '-k');
xlabel('Iteration'), ylabel('Training Accuracy'), title('Training accuracy for Multi-Class ANN');

testH = testX * weight1;
testH = 1 ./ (1 + exp(-1 .* testH));
        
testO = testH * weight2;
testO = 1 ./ (1 + exp(-1 .* testO));
ac=getAccuracy(testO, new_testY)*100;
fprintf("Testing accuracy: %f%%\n", ac);

