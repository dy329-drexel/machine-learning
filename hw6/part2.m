
%clear and specify data file
clear ; close all; 
filename = 'spambase.data';
datafile = 'spambase.mat';

if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file
    data = csvread(filename);

    save(datafile,'data');
end

% randomizes the data using the function

randomData=randomizeData(data);

colsize = size(randomData,2);
% selects the first 2/3 rd data for training and remaining 1/3 for testing
no = ceil( length(randomData) * 2 / 3 );
data_training = randomData(1 : no, :);

% set the remaining for testing
data_testing = randomData(no+1 : end, :);

%find mean and sd of the data
%find mean and sd of the data
mv = mean(data_training(:, 1:end-1));
sd = std(data_training(:, 1:end-1));

%select last column as label
testY = data_testing(:, colsize);
Y = data_training(:, colsize);


%standardize the data
stdData = data_training(: , 1:colsize-1);

testData = data_testing(: , 1:colsize-1);

stdData = (stdData-mv)./sd;
testData = (testData-mv)./sd;



%find length of the data

N = length(stdData);

% add bias to train and test data
stdData = [stdData, ones(N,1)];
X = stdData;
testData = [testData, ones(length(testData), 1)];


%set iterations and learning parameter
i = 1000;
learn = 0.5;
D = size(stdData,2);

hidden_layer_size = 20;

%since binary classificastion output node=1
K = 1;

%set weights randomly in range -1 , 1
R = [-1 1];
weight1 = rand(D, 20)*range(R)+min(R);
weight2 = rand(20, K)*range(R)+min(R);

for i = 1:i

    %Propagate data forward to get
    H = X * weight1;
    H = 1 ./ (1 + exp(-1 .* H));
    
    O = H * weight2;
    O = 1 ./ (1 + exp(-1 .* O));
    %Computer the error at the output layer 
    EOUT = Y - O;
    %Update the weights from hidden layer to output laye
    weight2 = weight2 + (learn/N) * H.' * EOUT;
    
    EHIDDEN = EOUT * weight2.' .* H .* (1 - H);
    %Compute the error at the hidden layer
    weight1 = weight1 + (learn/N) .* X.' * EHIDDEN;
    
   
   
end


tp = 0;
fp = 0;
fn = 0;
tn = 0;
precision = [];
recall = [];

% The Precision-Recall graph for threadhold in .1 interval
for i = 0:0.1:1
    H = testData*weight1;
    H = 1./(1+exp(-H));

    O = H*weight2;
    O = 1./(1+exp(-O));

    label = (O(:,1) > i);
    
    for j=1:size(label,1)
        if testY(j) == 1
            if label(j) == 1
                tp = tp + 1;
            else
                fn = fn + 1;
            end
        else
            if label(j) == 0
                tn = tn + 1;
            else
                fp = fp + 1;
            end
        end
    end
    
    precision = [precision; tp/(tp+fp)];
    recall = [recall; tp/(tp+fn)];
end
figure;
plot(precision, recall);
xlabel('Precision'), ylabel('Recall'), title('SPAM Detection');

