function [accuracy] = getAccuracyMultiClass(O, Y)
%To choose the class label find the output node with the largest value
    len = length(O);
    correct = 0;
    for i = 1:len
        r1 = O(i, :);
        r2 = Y(i, :);
        
        [~, idx1] = max(r1);
        [~, idx2] = max(r2);

        if(idx1 == idx2)
            correct = correct + 1;
        end        
    end
    accuracy = correct/len;
end