
clear ; close all; 

    filename = 'x06Simple.csv';
    datafile = 'x06Simple.mat';

if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file ignoring first row and first column of the
    % data
    data = csvread(filename,1,1);
    

  
end


data=randomizeData(data);
% selects the first 2/3 rd data for training and remaining 1/3 for testing
no = ceil( length(data) * 2 / 3 );
data_training = data(1 : no, :);

% set the remaining for testing
data_testing = data(no : end, :);

%Standardize the data except the last column
Y = data_training(:,end);
s_data=featureNormalize(data_training(: , 1:end-1));

mv = mean(data_training(:, 1:end-1));
sd = std(data_training(:, 1:end-1));

% standardizes data

data_testing = [(data_testing(:, 1:end-1) - mv) ./ sd, data_testing(:, end)];



%adding extra feature of 1
X1 = ones(size(s_data, 1),1);

newStd_data=[X1,s_data];

%calculate theta = ((X' * X )^(-1) )* X' * Y

theta=(newStd_data' * newStd_data) \newStd_data' * Y;
display(theta);


%calculation of RMSE


X2 = ones(size(data_testing, 1),1);

testX= [X2 data_testing(:, 1:end-1)];

%get prediction 

p=testX*theta;
mse = mean( (p - data_testing(:, end)).^2 );
rmse = sqrt(mse);

% print out the rmse
fprintf('RMSE is %f\n', rmse);



