function distance = calDistance(data1, data2)
% Compute the similarity/distance for two given data, using Manhattan Distance.

    len = length(data1);

% Compute Manhattan Distance for data1 & data2
distance = 0;
for i = 1 : len
    distance = distance + abs(data1(i) - data2(i));
end
end