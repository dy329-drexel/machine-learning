Homework 3
Cs 383
Deepak Yadav
2/9/2018
----------------------------------------------------------------------
homework3 is the pdf containing solution to theory question and RMSE values
run-  part2.m for part 2 for closed form linear aggression
run-  part3.m for part 3 S folds cross validation
----------------------------------------------------------------------------------------
you need these function files in same directory 
featureNormalize.m: it standardize the data set X
randomizeData.m: it randomize data with seed 0

-----------------------------------------------------------------------------

small known issue: For bigger S my script is not working for part 3

-----------------------------------------------------------------------------------------------------------------------------
I prvided all data/data folders in Hw3.zip folder .