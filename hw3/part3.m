clear ; close all; 

    filename = 'x06Simple.csv';
    datafile = 'x06Simple.mat';

if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file ignoring first row and first column of the
    % data
    data = csvread(filename,1,1);

end


%randomize the data
data = randomizeData(data);

%create S folds
S=5;
sizeoffold = ceil(length(data) / S);


Errors = [];

for i = 1 : S
    % Select fold i as testing data and the remaining folds as training data
    first = 1 + (i - 1) * sizeoffold;
    last = min(first + sizeoffold - 1, length(data));
    testData = data(first:last, :);
    trainData= [data(1:first-1, :); data(last+1:end, :)];
    
    % Standardizes the data based on the training data(except for the last column)
   
    [m,sd]=getMeanStd(trainData(:, 1:end-1));
    trainData= [(trainData(:, 1:end-1) - m) ./ sd, trainData(:, end)];
    testData = [(testData(:, 1:end-1) - m) ./ sd, testData(:, end)];
    
    % Train linear regression model
    x = [ones(size(trainData, 1), 1) trainData(:, 1:end-1)];
    y = trainData(:, end); 
    theta = (x' * x) \ x' * y; 
    
    %  Compute the squared error for each sample in the current testing fold

    test = [ones( size(testData, 1), 1 ) testData(:, 1:end-1)];
    p = test * theta;
  Errors = [Errors; (p - testData(:, end)).^2];

end

rmse = sqrt(mean(Errors));
fprintf('RMSE: %f\n', rmse);

% Standardizes using mean and sd
function [ m, stds] = getMeanStd(data)
   m = mean(data);
    stds = std(data);
end

