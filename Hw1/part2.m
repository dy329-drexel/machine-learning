% Read data file
filename = 'diabetes.csv';
fd = fopen(filename);
if (fd < 0)
    disp('file not found')
    return
end
% Initialize vars
rawdata = csvread(filename);
means = zeros(1,9);
sdevs = zeros(1,9);
sdevs(1) = 1;


for c = 2:9
    means(c) = mean(rawdata(:,c));
    sdevs(c) = std(rawdata(:,c));
end

% mean and sdev
meanArr = repmat(means, size(rawdata,1), 1);
sdevArr = repmat(sdevs, size(rawdata,1), 1);

% Standardize data
stdData = (rawdata - meanArr) ./ sdevArr;

% PCA
covMatrix = cov(stdData(:, 2:end));
[eigenVectors, eigenValues] = eig(covMatrix);
stdData = stdData(:, 2:end) * eigenVectors(:, 7:end);
features = rawdata(:, 1);

% Plot the graph
figure(1);
hold on;

dim1 = stdData(:, 1);
dim2 = stdData(:, 2);

plot(dim2(features== 1), dim1(features== 1), 'or');
plot(dim2(features==-1), dim1(features==-1), 'xb');
legend('+1','-1');
title('Dimensionality Reduction via PCA');

