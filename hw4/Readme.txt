Homework 4
Cs 383
Deepak Yadav
2/23/2018
----------------------------------------------------------------------
homework4 is the pdf containing solution to theory question and performance stats of KNN
run-  hw4.m for KNN program

----------------------------------------------------------------------------------------
you need these function files in same directory 

randomizeData.m: it randomize data with seed 0
calDistance.m: calculate manhatten distance between points
-----------------------------------------------------------------------------
no Issues make sure the data file spambase.data is in the same directory. Since KNN calculate distance for each point it runs 
little slow. So wait few seconds for results

-----------------------------------------------------------------------------------------------------------------------------
I prvided all data/data folders in Hw4.zip folder .