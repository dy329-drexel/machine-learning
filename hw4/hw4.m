
%clear and specify data file
clear ; close all; 
filename = 'spambase.data';
datafile = 'spambase.mat';



if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file
    data = csvread(filename);

    save(datafile,'data');
end




% randomizes the data using the function

data = randomizeData(data);
colsize = size(data,2);
% selects the first 2/3 rd data for training and remaining 1/3 for testing
no = ceil( length(data) * 2 / 3 );
data_training = data(1 : no, :);

% set the remaining for testing
data_testing = data(no+1 : end, :);
labels = data_training(:, colsize);

%find mean and sd of the data
mv = mean(data_training(:, 1:end-1));
sd = std(data_training(:, 1:end-1));

% standardizes data
std_data_training = [(data_training(:, 1:end-1) - mv) ./ sd, data_training(:, end)];
std_data_testing = [(data_testing(:, 1:end-1) - mv) ./ sd, data_testing(:, end)];


 % Specify K=5 and other performance variables , you can change the value
 % of k
 k=5;
 tp = 0;
 fp = 0;
 fn = 0;
 tn = 0;
 
 
% Declare the predict with all zeros meaning not spam
predictLabel = zeros(size(std_data_testing, 1));

% For each testing data
for i = 1 : size(std_data_testing, 1)
    testing = std_data_testing(i, :);

    % Compute similarity/distance to each training data
    dist = zeros(size(std_data_training, 1), 1);
    for j = 1 : size(std_data_training, 1)
        dist(j) = calDistance(testing(1:end-1), std_data_training(j, 1:end-1));
    end

    %sort the distance 
    
  [~, I] = sort(dist);

    % store class label for the closest k points
 
    arr=zeros(size(k, 1));
     for j = 1 : k
        arr(j)= std_data_training(I(j), end);   
     end
     
    %find mode class label
    m = mode(arr);
   
    % if Mode class is spam than change value in prediction matrix
  
    if m==1
        predictLabel(i) = 1;
    end
  
end




% Count performance of our prediction 

for i = 1 : length(predictLabel)
    if predictLabel(i) == 1
        if std_data_testing(i, end) == 1
            tp = tp + 1;
        else
            fp = fp + 1;
        end
    else
        if std_data_testing(i, end) == 0
            tn = tn + 1;
        else
            fn = fn + 1;
        end
    end
end

% computer performance stats
precision = tp / (tp + fp);
recall = tp / (tp + fn);
fmeasure = 2 * precision * recall / (precision + recall);
accuracy = (tp + tn) / (tp + tn + fp + fn);

% Print out the performance results
fprintf('Precision is: %f\n', precision);
fprintf('Recall is: %f\n', recall);
fprintf('F-Measure is: %f\n', fmeasure);
fprintf('Accuracy is : %f\n', accuracy);



