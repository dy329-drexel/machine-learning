function [] = plotkMeans(X,centroids,idx, i,xcol,ycol)    
palette =  [1 0 0
    0 0 1
    0 1 1
    1 1 0
    0 1 0
    1 0 1
    1 1 1
  ];


colors = palette(idx, :);

% Plot the data


scatter(X(:,ycol), X(:,xcol), 15,colors);

% Plot the centroids 
plot(centroids(:,ycol), centroids(:,xcol), 'x', ...
     'MarkerEdgeColor','k', ...
     'MarkerSize', 10, 'LineWidth', 3);



% Title
title(sprintf('Iteration number %d', i))

end
