 function [Centroid,idx] = kmeans(data, k, xcol, ycol)
data= featureNormalize(data);
initial_centroids=kMeansInitCentroids(data,k);

figure;
plot( data(:, ycol),data(:, xcol), 'rx');
hold on;

plot(initial_centroids(:, 2), initial_centroids(:, 1), 'bo', 'MarkerSize',10,'MarkerEdgeColor','b', 'MarkerFaceColor', 'b' );
title('Initial Seeds');



[Centroid,idx]=runkMeans(data,initial_centroids,9999,xcol,ycol);
 
 
 end