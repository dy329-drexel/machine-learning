

clear ; close all; 

    filename = 'diabetes.csv';
    datafile = 'diabetes.mat';

if(exist(datafile, 'file'))
    % load data file if it exit
    load(datafile);
else
    % load data from csv file
    data = csvread(filename);
    label=data(:,1);
    % select 6th and 7th feature of data
    data = data(:, 7:8);

  
end

[Centroid,idx]=kmeans(data,2,1,2);

%calculating purity
%total psoitive negative result
sum1=sum(label(:)==-1);
sum2=sum(label(:)==1);
disp(sum1);
disp(sum2);
%result in each cluster 1 is negative 2 is positive
sum3=sum(idx(:)==1);
disp(sum3);
sum4=sum(idx(:)==2);
disp(sum4);
%sum of individual purity
purity1=sum3/sum1;
purity2=sum4/sum2;
purity=purity1+purity2;
disp("purity is");
disp(purity);







