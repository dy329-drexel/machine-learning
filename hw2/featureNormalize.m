function [X_norm] = featureNormalize(X)

%   FEATURENORMALIZE(X) returns a normalized version of X where
%   the mean value of each feature is 0 and the standard deviation
%   is 1.

m = mean(X);
Xnorm = bsxfun(@minus, X, m);

sd = std(Xnorm);
X_norm = bsxfun(@rdivide, Xnorm, sd);
end