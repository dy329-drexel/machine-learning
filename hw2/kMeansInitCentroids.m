function centroids = kMeansInitCentroids(X, K)


centroids = zeros(K, size(X, 2));

% we should set centroids to randomly chosen examples from
%               the dataset X

rng(0);
abc = randperm(length(X));
centroids =X(abc(1:K),:);

end