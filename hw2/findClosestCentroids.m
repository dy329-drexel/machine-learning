function idx = findClosestCentroids(X, centroids)

K = size(centroids, 1);


idx = zeros(size(X,1), 1);

% Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.



for i = 1:length(X)
    minDist = Inf;
    for j = 1:K
        dist = norm(X(i,:) - centroids(j,:)) ^ 2;

        if dist < minDist
            minDist = dist;
            idx(i) = j;
        end
    end
end
end
