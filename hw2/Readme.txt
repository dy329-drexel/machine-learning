Homework 2
Cs 383
Deepak Yadav
2/3/2018
----------------------------------------------------------------------
homework1 is the pdf containing all the images  of part 2 and part4. and also purity calculation
run-  hw2.m for seeing the result i am calling function kmean(X,K,xcol,ycol);
uncomment the line %data = data(:, 7:8); for part 2 and 3 to give only 2 features 
for other parts you can comment it and give whole data to function.
----------------------------------------------------------------------------------------
you need these function files in same directory 
featureNormalize.m: it standardize the data set X
computeCentroids.m: it calculate the the centroids i.e. mean of all data points belongs to a center
kMeansInitCentroids.m: randomly initilize centroids i.e. clusters
runkMeans.m: it is my implementation of kMean if we pass data and intitial cluster, max iterations and xcol,ycol
kmeans.m: fucntion added to adapt to part4 it takes data, K, xcol,ycol and plot k means 
findClosestCentroids.m : this fucntion finds closest centroids .
plotkMeans.m : it plots a grpah providing clusters and data 
-----------------------------------------------------------------------------

small known issue: some how I tried changing the marker type during plotting the grpah but i was unable to change it with dyanic color palette. So i 
used different colors for data points but I used black X for all cluster centers
apart from it work fine.

-----------------------------------------------------------------------------------------------------------------------------
I prvided all data/data folders in Hw1.zip folder and for convenience I also added my screen shots of the output of my scripts. 