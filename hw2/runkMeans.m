 function [centroids, idx] = runkMeans(X, initial_centroids,max_iters,xcol,ycol)
                                 

    figure;
    hold on;


% Initialize values
[m n] = size(X);
K = size(initial_centroids, 1);
centroids = initial_centroids;
previous_centroids = centroids;
idx = zeros(m, 1);

% Run K-Means
for i=1:max_iters
    
   
    fprintf('K-Means iteration %d/%d...\n', i, max_iters);
    % For each example in X, assign it to the closest centroid
    idx = findClosestCentroids(X, centroids);
    previous_centroids = centroids;
    %  compute new centroids
    centroids = computeCentroids(X, idx, K);
    %break if we reach convergence 
     if sum(abs( previous_centroids(:)- centroids(:)))<eps
        disp(i);
        break
    end
end

% plot


 plotkMeans(X, centroids, idx, i,xcol,ycol);

end
